/****************************************************************************
 Module
   IRSensorService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TapeService.h"
#include "MotionService.h"
#include "MotorService.h"
#include "Initialization.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"

#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/

#define TAPE_DRIVE_TIME 250
#define LEFT_SPEED 95
#define RIGHT_SPEED 100

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static bool IsTape(void);
static void StopRotation(void);
static void DriveForward(void);
uint8_t ReadTapeSensor(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitTapeService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  InitReflectanceSensor();
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostTapeService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunTapeService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
  switch (ThisEvent.EventType)
  {
    case ES_DRIVE_UNTIL_TAPE:  //If drive until tape command
    { DriveForward();
      ES_Timer_InitTimer(TAPE_TIMER, TAPE_DRIVE_TIME);   //Start 250 ms timer
    }
    break;

    case ES_TIMEOUT: //If 250 ms timer is up
    { if ((ThisEvent.EventParam == ES_TIMEOUT) && (ThisEvent.EventParam == TAPE_TIMER))
      {
        ES_Event_t NewEvent;

        if (IsTape())    //If found tape
        {
          StopRotation();                     //Stop
          NewEvent.EventType = ES_FOUND_TAPE; //Post Event to relevant services
          PostMotionService(NewEvent);
          //puts("\r\n Found Tape");
        }
        else
        {
          NewEvent.EventType = ES_DRIVE_UNTIL_TAPE;   //Else, continue to druive
          PostTapeService(NewEvent);
        }
      }
    }
    break;

    case ES_STOP:     //If a Stop Comment
    { StopRotation(); //Immediately stop
    }
    break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

//Function to Check if Robot is aligned with Beacon
static bool IsTape(void)
{
  bool Rtn = false;

  if (ReadTapeSensor() != 0)  //Line is HI
  {
    Rtn = true; //Tape is found
  }

  return Rtn;
}

uint8_t ReadTapeSensor(void)
{
  uint8_t Rtn = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT7HI;
  return Rtn;
}

//Function to Stop rotating
static void StopRotation(void)
{
  SetPWM(CW, 0, 0);  //Send duty cycle of 0 to stop
  ES_Timer_StopTimer(TAPE_TIMER);
}

static void DriveForward(void)
{
  SetPWM(FORWARD, LEFT_SPEED, RIGHT_SPEED);
  //puts("\r\n Driving forward");
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
