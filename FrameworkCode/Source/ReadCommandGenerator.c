/****************************************************************************
 Module
   ReadCommandGenerator.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ReadCommandGenerator.h"
#include "Initialization.h"
#include "MotionService.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_ssi.h"

#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/

#define TicksPerMS 40000    //40MHz clock * 1000 ms/us
#define QUERY_VAL 0xAA
#define QueryTime 5 //5 ms query time

//#define NO_COMGEN

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

static bool SendCommand(ES_Event_t);
static void PostNow(ES_Event_t);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ReadComGenState_t CurrentState = WaitNextCommand;
enum Command
{
  Stop = 0x00, Rotate_CW_90 = 0x02, Rotate_CW_45 = 0x03, Rotate_CCW_90 = 0x04,
  Rotate_CCW_45 = 0x05, Drive_Forward_Half = 0x08, Drive_Forward_Full = 0x09,
  Drive_Reverse_Half = 0x10, Drive_Reverse_Full = 0x11, Align_With_Beacon = 0x20,
  Drive_Until_Tape = 0x40, Return_Val = 0xFF
};

static uint8_t CommandValue;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitReadComGen(uint8_t Priority)
{
  //globally disable interrupts
  __disable_irq();
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  //init spi
  InitSPI();
  __enable_irq();

  ES_Timer_SetTimer(QueryTimer, QueryTime);
  ES_Timer_StartTimer(QueryTimer);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostReadComGen(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunReadComGen(ES_Event_t ThisEvent)
{
  //printf("\n\r Read %d", CommandValue);
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  switch (CurrentState)
  {
    case WaitNextCommand:
    {
      if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == QueryTimer))
      {
        // send 0xAA to the command generator
        HWREG(SSI0_BASE + SSI_O_DR) = QUERY_VAL;
        //restart the timer
        ES_Timer_SetTimer(QueryTimer, QueryTime);
        ES_Timer_StartTimer(QueryTimer);
      }
      else if (ThisEvent.EventType == ES_QUERY_SENT)
      {
        if (SendCommand(ThisEvent))
        {
          CurrentState = WaitForFF;
        }
      }
    }
    break;
    case WaitForFF:
      //if the timer expires, send AA to the command generator
      if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == QueryTimer))
      {
        //send 0xAA to the command generator
        HWREG(SSI0_BASE + SSI_O_DR) = QUERY_VAL;
        ES_Timer_SetTimer(QueryTimer, QueryTime);
        //restart the timer
        ES_Timer_StartTimer(QueryTimer);
      }
      else if ((ThisEvent.EventType == ES_QUERY_SENT) && (ThisEvent.EventParam == Return_Val))
      {
        CurrentState = WaitNextCommand;
      }
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
ReadComGenState_t QueryReadComGen(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void ComGenQueryISR(void)
{
  // start by clearing the source of the interrupt
  HWREG(SSI0_BASE + SSI_O_ICR) = SSI_ICR_EOTIC;

  uint8_t ReadVal = HWREG(SSI0_BASE + SSI_O_DR);
  CommandValue = ReadVal;

  ES_Event_t NewEvent;
  NewEvent.EventType = ES_QUERY_SENT;
  NewEvent.EventParam = ReadVal;
  PostReadComGen(NewEvent);
}

bool SendCommand(ES_Event_t ThisEvent)
{

  bool RTN = false;

  if (ThisEvent.EventParam == Stop)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_STOP;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Rotate_CW_90)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_ROTATE_CW_90;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Rotate_CW_45)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_ROTATE_CW_45;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Rotate_CCW_90)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_ROTATE_CCW_90;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Rotate_CCW_45)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_ROTATE_CCW_90;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Drive_Forward_Half)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_DRIVE_FORWARD_HALF;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Drive_Forward_Full)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_DRIVE_FORWARD_FULL;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Drive_Reverse_Half)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_DRIVE_REVERSE_HALF;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Drive_Reverse_Full)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_DRIVE_REVERSE_FULL;
    PostNow(NewEvent);
    RTN = true;
  }
  else if (ThisEvent.EventParam == Drive_Until_Tape)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_DRIVE_UNTIL_TAPE;
    PostNow(NewEvent);
    ;
    RTN = true;
  }
  else if (ThisEvent.EventParam == Align_With_Beacon)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_ALIGN_WITH_BEACON;
    PostNow(NewEvent);
    RTN = true;
  }

  return RTN;
}

void PostNow(ES_Event_t ThisEvent)
{
  #ifndef NO_COMGEN
  PostMotionService(ThisEvent);
  #endif

  printf("\n\r Execute: %d", CommandValue);
}