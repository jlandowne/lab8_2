/****************************************************************************
 Module
   MotorService.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/04/19 08:58 mrl     write test code to determine how long motors should run
 02/03/19 11:37 mrl     remove control ISR (decided to not use encoders)
 02/03/19 10:04 mrl     test with Connor's initialization service
 02/02/19 15:19 mrl     update after receiving hardware
 02/01/19 18:59 mrl     copy TestHarnessService0 into this file
****************************************************************************/
//#define TEST
/*----------------------------- Include Files -----------------------------*/
// This module
#include "MotorService.h"

// Hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

// Other
#include "Initialization.h"
#include "hw_pwm.h"

/*----------------------------- Module Defines ----------------------------*/
#ifdef TEST
  #define LEFT_FULL_FORWARD 90  // DC for left wheel "full speed" forward
  #define LEFT_FULL_BACKWARD 95
  #define RIGHT_FULL 100
  #define LEFT_HALF_FORWARD 42
  #define LEFT_HALF_BACKWARD 55
  #define RIGHT_HALF 50

  #define NINETY_TURN_TIME 1025
#endif

/*---------------------------- Module Functions ---------------------------*/
static void SetSystemRotation(uint8_t RotationDirection);
static void LeftDirection(uint8_t WheelRotationDirection);
static void RightDirection(uint8_t WheelRotationDirection);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static uint8_t  LeftDuty;
static uint8_t  RightDuty;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMotorService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any other required initialization for
     this service

 Notes

 Author
     Marissa Lee, 02/01/19, 19:13
****************************************************************************/
bool InitMotorService(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  __disable_irq();
  // PB0, PB1 are digital I/O (connected to green leads of motors); PB6, PB7 are PWM
  InitMotorGPIO();

  // initialize PWM lines
  InitPWM(); // from Initialize.h
  __enable_irq();

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMotorService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Notes

 Author
Marissa Lee, 02/01/19, 19:17
****************************************************************************/
bool PostMotorService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMotorService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   TODO

 Notes

 Author
   Marissa Lee, 02/01/19, 19:17
****************************************************************************/
ES_Event_t RunMotorService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      #ifdef TEST
      puts("Running\r\n");
      // rotate CW by 90 degrees
      SetPWM(CW, LEFT_FULL_FORWARD, RIGHT_FULL);
      ES_Timer_InitTimer(MotorTestTimer, NINETY_TURN_TIME);

      // rotate CW by 45 degrees
      //      SetPWM(CW,LEFT_FULL_FORWARD,RIGHT_FULL);
      //      ES_Timer_InitTimer(MotorTestTimer, NINETY_TURN_TIME/2);

      // rotate CCW by 90 degrees
      //      SetPWM(CCW,LEFT_FULL_FORWARD,RIGHT_FULL);
      //      ES_Timer_InitTimer(MotorTestTimer, NINETY_TURN_TIME);

      // rotate CCW by 45 degrees
      //      SetPWM(CCW,LEFT_FULL_FORWARD,RIGHT_FULL);
      //      ES_Timer_InitTimer(MotorTestTimer, NINETY_TURN_TIME/2);

      // drive forward half speed
      //      SetPWM(FORWARD,LEFT_HALF_FORWARD,RIGHT_HALF);
      //      ES_Timer_InitTimer(MotorTestTimer, 10000);

      // drive forward full speed
      //      SetPWM(FORWARD,LEFT_FULL_FORWARD,RIGHT_FULL);
      //      ES_Timer_InitTimer(MotorTestTimer, 3000);

      // drive backward half speed
      //      SetPWM(BACKWARD,LEFT_HALF_BACKWARD,RIGHT_HALF);
      //      ES_Timer_InitTimer(MotorTestTimer, 5000);

      // drive backward full speed
      //      SetPWM(BACKWARD,LEFT_FULL_BACKWARD,RIGHT_FULL);
      //      ES_Timer_InitTimer(MotorTestTimer, 3000);
      #endif
    }
    break;

    #ifdef TEST
    case ES_TIMEOUT:
    {
      if (ThisEvent.EventParam == MotorTestTimer)
      {
        SetPWM(FORWARD, 0, 0);
        puts("Stopped\r\n");
      }
    }
    #endif
    default:
    {}
    break;
  }
  return ReturnEvent;
}

/* set PWM */
void SetPWM(uint8_t RotationDirection, uint8_t LeftDutyInput, uint8_t RightDutyInput)
{
  LeftDuty = LeftDutyInput;
  RightDuty = RightDutyInput;
  SetSystemRotation(RotationDirection);
  // left wheel PWM
  if (LeftDuty == 0)
  {
    HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;
  }
  else if (LeftDuty == 100)
  {
    HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ONE;
  }
  else
  {
    HWREG(PWM0_BASE + PWM_O_0_GENA) = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO);
    HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) - ((PWMTicksPerMS * PeriodInMS) * LeftDuty / 200);
  }

  // right wheel PWM
  if (RightDuty == 0)
  {
    HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;
  }
  else if (RightDuty == 100)
  {
    HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ONE;
  }
  else
  {
    HWREG(PWM0_BASE + PWM_O_0_GENB) = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO);
    HWREG(PWM0_BASE + PWM_O_0_CMPB) = HWREG(PWM0_BASE + PWM_O_0_LOAD) - ((PWMTicksPerMS * PeriodInMS) * RightDuty / 200);
  }
}

/***************************************************************************
 private functions
 ***************************************************************************/
/* set system rotation direction */
static void SetSystemRotation(uint8_t RotationDirection)
{
  if (RotationDirection == FORWARD) // forward
  {
    LeftDirection(CCW);
    RightDirection(CW);
  }
  else if (RotationDirection == CW) // rotate system CW
  {
    LeftDirection(CCW);
    RightDirection(CCW);
  }
  else if (RotationDirection == CCW) // rotate system CCW
  {
    LeftDirection(CW);
    RightDirection(CW);
  }
  else if (RotationDirection == BACKWARD) // backward
  {
    LeftDirection(CW);
    RightDirection(CCW);
  }
  #ifdef TEST
  printf("%d %d\r\n", LeftDuty, RightDuty);
  #endif
}

/* set left wheel rotation direction
    input: CW or CCW
*/
static void LeftDirection(uint8_t WheelRotationDirection)
{
  if (WheelRotationDirection == CW) // rotate wheel CW
  {
    // set PB0 high
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
    // correct duty
    LeftDuty = 100 - LeftDuty;
  }
  else if (WheelRotationDirection == CCW) // rotate wheel CCW
  {
    // set PB0 low
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
  }
}

/* set right wheel rotation direction
    input: CW or CCW
*/
static void RightDirection(uint8_t WheelRotationDirection)
{
  if (WheelRotationDirection == CW) // rotate wheel CW
  {
    // set PB1 high
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
    // correct duty
    RightDuty = 100 - RightDuty;
  }
  else if (WheelRotationDirection == CCW) // rotate wheel CCW
  {
    // set PB1 low
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO;
  }
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
