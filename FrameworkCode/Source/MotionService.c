/****************************************************************************
 Module
   MotionService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "MotionService.h"
#include "MotorService.h"
#include "IRService.h"
#include "TapeService.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"

#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/
// rotation times in timer ticks
#define ROTATE_TIME_90 1025
#define ROTATE_TIME_45 512

// duty cycles for left and right motors
#define LEFT_FULL_FWD 90
#define LEFT_FULL_BKWD 95
#define RIGHT_FULL_FWD 100
#define RIGHT_FULL_BKWD 100
#define LEFT_HALF_FWD 42
#define LEFT_HALF_BKWD 55
#define RIGHT_HALF_FWD 50
#define RIGHT_HALF_BKWD 50

// in verbose mode, print transitions for debugging purposes
#define VERBOSE

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MotionState_t  CurrentState;
static ES_Event_t     MotionDeferralQueue[3 + 1]; // can defer 3 events to this queue

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMotionService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMotionService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  // initialise deferral queue to store events that can't be treated immediately
  ES_InitDeferralQueueWith(MotionDeferralQueue, ARRAY_SIZE(MotionDeferralQueue));
  // put us into the Initial PseudoState
  CurrentState = InitPState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMotionService

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMotionService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMotionService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMotionService(ES_Event_t ThisEvent)
{
  ES_Event_t  ReturnEvent;
  ES_Event_t  EventToPost;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitPState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // robot starts with all motors idle
        CurrentState = Stopped;
        SetPWM(CW, 0, 0);
        #ifdef VERBOSE
        puts("\n\rMotion Init\r\nStopping\r");
        #endif
      }
    }
    break;

    case Rotating:
    {
      // respond to a stop commmand or an end of rotation
      if ((ThisEvent.EventType == ES_STOP)
          || ((ThisEvent.EventType == ES_TIMEOUT)
          && (ThisEvent.EventParam == ROTATE_TIMER)))
      {
        // stop motors
        SetPWM(CW, 0, 0);
        // recall deferred commands
        ES_RecallEvents(MyPriority, MotionDeferralQueue);
        // Decide what the next state will be
        CurrentState = Stopped;
        #ifdef VERBOSE
        puts("\n\rStopping rotation\r");
        #endif
      }
      else
      {
        // defer event until end of rotation
        ES_DeferEvent(MotionDeferralQueue, ThisEvent);
        #ifdef VERBOSE
        printf("\n\rDeferred event %d\r", ThisEvent.EventParam);
        #endif
      }
    }
    break;

    case DrivingStraight:
    {
      // respond to any commmand by stopping motors
      SetPWM(CW, 0, 0);
      // and reposting to self
      PostMotionService(ThisEvent);
      // Decide what the next state will be
      CurrentState = Stopped;
      #ifdef VERBOSE
      puts("\n\rStopping\r");
      #endif
    }
    break;

    case AligningWithBeacon:
    {
      if (ThisEvent.EventType == ES_STOP)
      {
        // stop seraching for beacon
        PostIRService(ThisEvent);
        // recall deferred commands
        ES_RecallEvents(MyPriority, MotionDeferralQueue);
        // Decide what the next state will be
        CurrentState = Stopped;
        #ifdef VERBOSE
        puts("\n\rStopping\r");
        #endif
      }
      // if aligned with beacon
      else if (ThisEvent.EventType == ES_ALIGNED)
      {
        // recall deferred commands
        ES_RecallEvents(MyPriority, MotionDeferralQueue);
        ES_Event_t StopEvent; //Stop motor in IR Service
        StopEvent.EventType = ES_STOP;
        PostIRService(StopEvent);
        // Decide what the next state will be
        CurrentState = Stopped;
        #ifdef VERBOSE
        puts("\n\rBeacon found\r\nStopping\r");
        #endif
      }
      else
      {
        // defer event until aligned with beacon
        ES_DeferEvent(MotionDeferralQueue, ThisEvent);
        #ifdef VERBOSE
        printf("\n\rDeferred event %d\n\r", ThisEvent.EventType);
        #endif
      }
    }
    break;

    case DrivingToTape:
    {
      if (ThisEvent.EventType == ES_STOP)
      {
        // stop driving to tape
        PostTapeService(ThisEvent);
        // recall deferred commands
        ES_RecallEvents(MyPriority, MotionDeferralQueue);
        // Decide what the next state will be
        CurrentState = Stopped;
        #ifdef VERBOSE
        puts("\n\rStopping\r");
        #endif
      }
      // if tape found
      else if (ThisEvent.EventType == ES_FOUND_TAPE)
      {
        // recall deferred commands
        ES_RecallEvents(MyPriority, MotionDeferralQueue);
        ES_Event_t StopEvent; //Stop motor in Tape Service
        StopEvent.EventType = ES_STOP;
        PostTapeService(StopEvent);
        // Decide what the next state will be
        CurrentState = Stopped;
        #ifdef VERBOSE
        puts("\n\rFound tape\r\nStopping\r");
        #endif
      }
      else
      {
        // defer event until tape is found
        ES_DeferEvent(MotionDeferralQueue, ThisEvent);
        #ifdef VERBOSE
        printf("\n\rDeferred event %d\n\r", ThisEvent.EventType);
        #endif
      }
    }
    break;

    case Stopped:
    {
      switch (ThisEvent.EventType)
      {
        case ES_STOP:
        {
          // stop motors
          SetPWM(CW, 0, 0);
          // Decide what the next state will be
          CurrentState = Stopped;
          #ifdef VERBOSE
          puts("\n\rStopping\r");
          #endif
        }
        break;

        case ES_ROTATE_CW_90:
        {
          // start motors
          SetPWM(CW, LEFT_FULL_FWD, RIGHT_FULL_FWD);
          // start rotation timer
          ES_Timer_InitTimer(ROTATE_TIMER, ROTATE_TIME_90);
          // Decide what the next state will be
          CurrentState = Rotating;
          #ifdef VERBOSE
          puts("\n\rRotating 90 CW\r");
          #endif
        }
        break;

        case ES_ROTATE_CW_45:
        {
          // start motors
          SetPWM(CW, LEFT_FULL_FWD, RIGHT_FULL_FWD);
          // start rotation timer
          ES_Timer_InitTimer(ROTATE_TIMER, ROTATE_TIME_45);
          // Decide what the next state will be
          CurrentState = Rotating;
          #ifdef VERBOSE
          puts("\n\rRotating 45 CW\r");
          #endif
        }
        break;

        case ES_ROTATE_CCW_90:
        {
          // start motors
          SetPWM(CCW, LEFT_FULL_FWD, RIGHT_FULL_FWD);
          // start rotation timer
          ES_Timer_InitTimer(ROTATE_TIMER, ROTATE_TIME_90);
          // Decide what the next state will be
          CurrentState = Rotating;
          #ifdef VERBOSE
          puts("\n\rRotating 90 CCW\r");
          #endif
        }
        break;

        case ES_ROTATE_CCW_45:
        {
          // start motors
          SetPWM(CCW, LEFT_FULL_FWD, RIGHT_FULL_FWD);
          // start rotation timer
          ES_Timer_InitTimer(ROTATE_TIMER, ROTATE_TIME_45);
          // Decide what the next state will be
          CurrentState = Rotating;
          #ifdef VERBOSE
          puts("\n\rRotating 45 CCW\r");
          #endif
        }
        break;

        case ES_DRIVE_FORWARD_HALF:
        {
          // start motors
          SetPWM(FORWARD, LEFT_HALF_FWD, RIGHT_HALF_FWD);
          // Decide what the next state will be
          CurrentState = DrivingStraight;
          #ifdef VERBOSE
          puts("\n\rDriving forward at half speed\r");
          #endif
        }
        break;

        case ES_DRIVE_FORWARD_FULL:
        {
          // start motors
          SetPWM(FORWARD, LEFT_FULL_FWD, RIGHT_FULL_FWD);
          // Decide what the next state will be
          CurrentState = DrivingStraight;
          #ifdef VERBOSE
          puts("\n\rDriving forward at full speed\r");
          #endif
        }
        break;

        case ES_DRIVE_REVERSE_HALF:
        {
          // start motors
          SetPWM(BACKWARD, LEFT_HALF_BKWD, RIGHT_HALF_BKWD);
          // Decide what the next state will be
          CurrentState = DrivingStraight;
          #ifdef VERBOSE
          puts("\n\rDriving backward at half speed\r");
          #endif
        }
        break;

        case ES_DRIVE_REVERSE_FULL:
        {
          // start motors
          SetPWM(BACKWARD, LEFT_FULL_BKWD, RIGHT_FULL_BKWD);
          // Decide what the next state will be
          CurrentState = DrivingStraight;
          #ifdef VERBOSE
          puts("\n\rDriving backward at full speed\r");
          #endif
        }
        break;

        case ES_ALIGN_WITH_BEACON:
        {
          // ask IR service to search for IR beacon
          EventToPost.EventType = ES_ALIGN_WITH_BEACON;
          PostIRService(EventToPost);
          // Decide what the next state will be
          CurrentState = AligningWithBeacon;
          #ifdef VERBOSE
          puts("\n\rAligning with beacon\r");
          #endif
        }
        break;

        case ES_DRIVE_UNTIL_TAPE:
        {
          // ask tape service to search for the black tape
          EventToPost.EventType = ES_DRIVE_UNTIL_TAPE;
          PostTapeService(EventToPost);
          // Decide what the next state will be
          CurrentState = DrivingToTape;
          #ifdef VERBOSE
          puts("\n\rDriving to tape\r");
          #endif
        }
        break;

        // repeat cases as required for relevant events
        default:
          ;
      } // end switch on CurrentEvent
    }   // end case Stopped:
    break;

    // repeat state pattern as required for other states
    default:
      ;
  } // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
/*TemplateState_t QueryTemplateFSM(void)
{
  return CurrentState;
}*/

/***************************************************************************
 private functions
 ***************************************************************************/
