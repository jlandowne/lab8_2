#include "ES_Configure.h"
#include "ES_Framework.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"

#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"

#include "Initialization.h"

//enable two PWM lines
void InitPWM(void)
{
  //using PB6 and PB7
  //enable clock to PWM Module 0 (both on PWM Module 0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  //system clock/32 to get longer pulse widths per tick
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
  //wait for clock to startup
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {}
  //disable the PWM0 Generator Block 0 (6 and 7) while initializing
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;
  //1 on rising compare A, 0 on falling compare A for GenBlock 0 (6 and 7)
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;
  //Set the PWM period. Period = 2*Load
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PeriodInMS * PWMTicksPerMS) >> 1); //change shift value based on divisor
  //set initial duty cycle to 50%
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD);
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = HWREG(PWM0_BASE + PWM_O_0_LOAD);
  //Enable PWM0 and PWM1
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN);
  //alternate select PB6 and PB7 for PWM
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT6HI | BIT7HI);
  //map PWM to PB6 and PB7 Mux value of 4
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0xf0ffffff) + (4 << (6 * BitsPerNibble));
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x0fffffff) + (4 << (7 * BitsPerNibble));
  //enable PB6 and PB7 for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT6HI | BIT7HI);
  //enable PB6 and PB7 for output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT6HI | BIT7HI);
  //set up/down count mode, enable the PWM generator and make locally synchronized
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE | PWM_0_CTL_GENAUPD_LS) | PWM_0_CTL_GENBUPD_LS;
}

//enable 2 GPIO pins for motor stuff
void InitMotorGPIO(void)
{
  //Port B already initialized in PWM code
  //using PB0 and PB1 for the L293NE
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI);
}

//Init SPI
void InitSPI(void)
{
//1. Enable the clock to the GPIO port (Port A)
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
//2. Enable the clock to SSI module 0 (PA2, PA3, PA4, PA5)
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;
//3. Wait for the GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
  {}
//4. Program the GPIO to use the alternate functions on the SSI pins
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
//5. Set mux position in GPIOPCTL to select the SSI use of the pins
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) |= (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) +
      ((2 << (2 * BitsPerNibble)) + (2 << (3 * BitsPerNibble)) + (2 << (4 * BitsPerNibble)) + (2 << (5 * BitsPerNibble)));
//6. Program the port lines for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
//7. Program the required data directions on the port lines
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
//8. If using SPI mode 3, program the pull-up on the clock line (using SPI)
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
//9. Wait for the SSI0 to be ready
  while ((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R0) != SYSCTL_PRSSI_R0)
  {}
//10. Make sure that the SSI is disabled before programming mode bits
//SSI_CR1_SSE = 10 (SSE is the third bit; 0 is disabled, 1 is enabled)
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;
//11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
//SSI_CR1_MS = 100 (MS is the third bit; 0 is master, 1 is slave)
//SSI_CR1_EOT = 10000 (EOT is fifth bit; 1 means EOT interrupt)
  HWREG(SSI0_BASE + SSI_O_CR1) = (HWREG(SSI0_BASE + SSI_O_CR1 & ~SSI_CR1_MS)) | SSI_CR1_EOT;
//SSI_RIS_TXRIS  = 1000 (TXRIS is the fourth bit; 1 means interrupt is EOT if EOT bit set)
  HWREG(SSI0_BASE + SSI_O_RIS) |= SSI_RIS_TXRIS;
//12. Configure the SSI clock source to the system clock
//SSI_CC_CS_SYSPLL = 0x00000000, which is for system clock
  HWREG(SSI0_BASE + SSI_O_CC) = SSI_CC_CS_SYSPLL;
//13. Configure the clock pre-scaler
//40 MHz / 50 = 800 kHz
  HWREG(SSI0_BASE + SSI_O_CPSR) = 50;
//14. Configure clock rate (SCR), phase & polarity (SPH, SPO), mode (FRF), data size (DSS)
//SSI_CR0_SCR_M = 1111111100000000 (want to set to zero)
  HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_SCR_M;
//SSI_CR0_SPH = 10000000 (1 is second clock edge data capture)
//SSI_CR0_SPO = 01000000 (1 is steady state hi for the clock)
//SSI_CR0_FRF_MOTO = 0x00000000 (for freescale SPI set to zero)
//SSI_CR0_DSS_8 = 111 (for 8-bit data size)
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & SSI_CR0_FRF_MOTO) | (SSI_CR0_DSS_8 | SSI_CR0_SPH | SSI_CR0_SPO);
//15. Locally enable interrupts (TXIM in SSIIM)
//SSI_IM_TXIM = 1000;
//SSI_IM_RXIM = 100
  HWREG(SSI0_BASE + SSI_O_IM) |= (SSI_IM_TXIM | SSI_IM_RXIM);
//16. Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
//17. Enable the NVIC interrupt for the SSI when starting to transmit
  HWREG(NVIC_EN0) = BIT7HI;
}

//Init IR Sensor

//Init Black Tape/Reflectance Sensor
void InitReflectanceSensor(void)
{
  //using PC7
  //enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  //wait for Port C to initialize
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2)
  {}
  //set up PC7 as digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT7HI;
  //set PC7 as digital input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT7LO;
}

void InitQueryPeriodicTimer(void)
{
  //PC5: Wide Timer 0 Timer B
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  // kill a few cycles to let the clock get going
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0)
  {}
  // make sure that timer (Timer B) on WideTimer0 is disabled before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // set up timer A in periodic mode so that it repeats the time-outs
  HWREG(WTIMER0_BASE + TIMER_O_TBMR) = (HWREG(WTIMER0_BASE + TIMER_O_TBMR) & ~TIMER_TBMR_TBMR_M) | TIMER_TBMR_TBMR_PERIOD;
  // set timeout here
  HWREG(WTIMER0_BASE + TIMER_O_TBILR) = PeriodicQuery; //2 mS timeout (40000 ticks/mS)
  // enable a local timeout interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
  // enable the Timer B in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 95 so appears in EN2 at bit 31
  HWREG(NVIC_EN2) |= BIT31HI;
  //keep priority lower than input capture stuff so that edges aren't missed
  HWREG(NVIC_PRI23) = (HWREG(NVIC_PRI23) & ~NVIC_PRI23_INTC_M) | (1 < NVIC_PRI23_INTC_S);
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

void InitMotorPeriodicTimer(void)
{
  //PC6: Wide Timer 1 Timer A
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  // kill a few cycles to let the clock get going
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1)
  {}
  // make sure that timer (Timer A) on WideTimer1 is disabled before configuring
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER1_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // set up timer A in periodic mode so that it repeats the time-outs
  HWREG(WTIMER1_BASE + TIMER_O_TAMR) = (HWREG(WTIMER1_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAMR_M) | TIMER_TAMR_TAMR_PERIOD;
  // set timeout here
  HWREG(WTIMER1_BASE + TIMER_O_TAILR) = PeriodicMotor; //period defined above
  // enable a local timeout interrupt
  HWREG(WTIMER1_BASE + TIMER_O_IMR) |= TIMER_IMR_TATOIM;
  // enable the Timer A in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 96 so appears in EN3 at bit 0
  HWREG(NVIC_EN3) |= BIT0HI;
  //keep priority lower than input capture stuff so that edges aren't missed
  HWREG(NVIC_PRI24) = (HWREG(NVIC_PRI24) & ~NVIC_PRI24_INTA_M) | (1 < NVIC_PRI24_INTA_S);
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

void InitInputCapturePeriod(void)
{
  //using PC4 for input capture
  //enable clock to timer
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  //enable clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  //disable Timer A before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  //config timer for 32-bit mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  //use full interval load 32-bit register for Timer A
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xffffffff;
  //set up Timer A in capture mode (TAMR=3, TAAMS = 0) for edge timer (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= TIMER_CTL_TAEVENT_BOTH;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT0CCP0, 16 to shift it over to the
  // right nibble for bit 4 (4 bits/nibble * 4 bits)
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) = ((HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xfff0ffff) + (7 << (4 * BitsPerNibble)));
  // Enable pin 4 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI;
  // make pin 4 on Port C into an input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;
  // back to the timer to enable a local capture interrupt for Timer A
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94 so appears in EN2 at bit 30
  HWREG(NVIC_EN2) |= (BIT30HI);
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger for Timer A and Timer B
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}
