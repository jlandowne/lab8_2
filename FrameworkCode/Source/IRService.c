/****************************************************************************
 Module
   IRSensorService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "IRService.h"
#include "MotorService.h"
#include "MotionService.h"
#include "Initialization.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"

#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/

#define ALIGN_TIME 250
#define TICKS_PER_MS 40000
#define LEFT_SPEED 40
#define RIGHT_SPEED 40

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

bool IsAligned(void);
static void Rotate(void);
static void StopRotation(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static uint32_t LastCapture = 0;
static uint32_t Period = 1;
static uint32_t Frequency;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitIRService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  InitInputCapturePeriod();
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostIRService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunIRService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
  switch (ThisEvent.EventType)
  {
    case ES_ALIGN_WITH_BEACON:                      //If Align with Beacon Event
    { ES_Timer_InitTimer(ALIGN_TIMER, ALIGN_TIME);  //Start 250 ms timer
      Rotate();                                     //Start to rotate CW
    }
    break;

    case ES_TIMEOUT: //If 250 ms timer is up
    { if (ThisEvent.EventParam == ALIGN_TIMER)
      {
        ES_Event_t NewEvent;
        if (IsAligned())    //If Aligned with Beacon
        {
          StopRotation();                   //Stop
          NewEvent.EventType = ES_ALIGNED;  //Post Event to relevant services
          PostMotionService(NewEvent);
          //puts("\r\n Aligned");
        }
        else
        {
          NewEvent.EventType = ES_ALIGN_WITH_BEACON;   //Else, continue to rotate
          PostIRService(NewEvent);
        }
      }
    }
    break;

    case ES_STOP:     //If a Stop Comment
    { StopRotation(); //Immediately stop
    }
    break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

//Function to Check if Robot is aligned with Beacon
bool IsAligned(void)
{
  bool RtnVal = false;
  
  //printf("\r\n %d",Frequency);

  if ((Frequency > 1000) && (Frequency < 1700))  //If Period is within Range
  {
    RtnVal = true; //Return true, else return false
  }

  return RtnVal;
}

//Function to rotate to find Beacon
static void Rotate(void)
{
  SetPWM(CW, LEFT_SPEED, RIGHT_SPEED);
  //puts("\r\n Rotating");
}

//Function to Stop rotating
static void StopRotation(void)
{
  ES_Timer_StopTimer(ALIGN_TIMER);
  SetPWM(CW, 0, 0);             //Send duty cycle of 0 to stop
}

//ISR for input capture
void InputCaptureResponse(void)
{
  uint32_t ThisCapture;  //Create ThisCapture to track current time of interrupt

// start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  //Grab captured value of interrupt time
  ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TAR);

  if ((HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT4HI) == BIT4HI)  // If Pin is HI (start of High Time Period)
  {
    LastCapture = ThisCapture; //Update Last Capture
  }
  else   //If Pin is LO (end of High Time Period)
  {
    Period = 2 * (ThisCapture - LastCapture); //Update Period as Time of Fall - Time of Rise
  }

  Frequency = (1000 * TICKS_PER_MS) / Period;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
