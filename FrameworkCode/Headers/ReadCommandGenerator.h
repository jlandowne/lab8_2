/****************************************************************************

  Header file for Read Command Generator Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef ReadCommandGenerator_H
#define ReadCommandGenerator_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"
#include <stdint.h>
#include <stdio.h> 

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  WaitNextCommand, WaitForFF,
}ReadComGenState_t;


// Public Function Prototypes

bool InitReadComGen(uint8_t Priority);
bool PostReadComGen(ES_Event_t ThisEvent);
ES_Event_t RunReadComGen(ES_Event_t ThisEvent);
ReadComGenState_t QueryReadComGen(void);

#endif /*  */

