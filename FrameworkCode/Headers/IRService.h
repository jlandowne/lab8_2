/****************************************************************************

  Header file for IRService
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef IRService_H
#define IRService_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

// Public Function Prototypes

bool InitIRService(uint8_t Priority);
bool PostIRService(ES_Event_t ThisEvent);
ES_Event_t RunIRService(ES_Event_t ThisEvent);

#endif /* IRService_H */

