/****************************************************************************

  Header file for MotionService
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MOTION_SERVICE_H
#define MOTION_SERVICE_H

#include <stdint.h>
#include <stdbool.h>
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitPState,
  Stopped,
  Rotating,
  DrivingStraight,
  AligningWithBeacon,
  DrivingToTape
} MotionState_t;

// Public Function Prototypes
bool InitMotionService(uint8_t Priority);
bool PostMotionService(ES_Event_t ThisEvent);
ES_Event_t RunMotionService(ES_Event_t ThisEvent);

#endif /* MOTION_SERVICE_H */

