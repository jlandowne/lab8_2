/****************************************************************************

  Header file for IRService
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef TapeService_H
#define TapeService_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

// Public Function Prototypes

bool InitTapeService(uint8_t Priority);
bool PostTapeService(ES_Event_t ThisEvent);
ES_Event_t RunTapeService(ES_Event_t ThisEvent);

#endif /* IRService_H */

