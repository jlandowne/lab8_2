/****************************************************************************

  Header file for MotorService
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MotorService_H
#define MotorService_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

// Public Function Prototypes
bool InitMotorService(uint8_t Priority);
bool PostMotorService(ES_Event_t ThisEvent);
ES_Event_t RunMotorService(ES_Event_t ThisEvent);
void SetPWM(uint8_t RotationDirection, uint8_t LeftDuty, uint8_t RightDuty);

// for setting wheel rotation direction
#define FORWARD 0
#define CW 1
#define CCW 2
#define BACKWARD 3

#endif /* MotorService_H */

