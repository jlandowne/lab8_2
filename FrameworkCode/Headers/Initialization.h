#ifndef Initialization_H
#define Initialization_H

#include "ES_Types.h"
#include "ES_Events.h"
#include "inc/hw_pwm.h"

#define PeriodInMS 1 //desired period is 250 uS, so load value is divided by 4 then by 2 since it's centered
//(40/32)*1000
#define PWMTicksPerMS 1250 //based on divisor value
#define BitsPerNibble 4
#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO) //change this as necessary
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO) //change this as necessary
#define PeriodicQuery 5 //period in ticks to query the command generator
#define PeriodicMotor 5 //period in ticks to send control to the motor
  
void InitPWM(void);
void InitMotorGPIO(void);
void InitSPI(void);
void InitReflectanceSensor(void);
void InitQueryPeriodicTimer(void);
void InitMotorPeriodicTimer(void);
void InitInputCapturePeriod(void);
  
#endif
